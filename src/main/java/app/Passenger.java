package app;

import java.io.Serializable;

public class Passenger implements Serializable{
        final int passengerId;
        final String passengerName;
        final String passengerSurname;

    public Passenger(int passengerId, String passengerName, String passengerSurname){
        this.passengerId = passengerId;
        this.passengerName = passengerName;
        this.passengerSurname = passengerSurname;
    }

    public String passengerProvide(){
        return String.format("%d:%s:%s", passengerId, passengerName, passengerSurname);
    }
}



